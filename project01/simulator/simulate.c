/* LC-2K Instruction-level simulator */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define NUMMEMORY 65536 /* maximum number of words in memory */
#define NUMREGS 8       /* number of machine registers */
#define MAXLINELENGTH 1000

typedef struct stateStruct {
  int pc;
  int mem[NUMMEMORY];
  int reg[NUMREGS];
  int numMemory;
} stateType;
typedef unsigned int inst_t;
typedef enum OPCODE {
  /* R-type */ ADD,
  /* R-type */ NOR,
  /* I-type */ LW,
  /* I-type */ SW,
  /* I-type */ BEQ,
  /* J-type */ JALR,
  /* O-type */ HALT,
  /* O-type */ NOOP,
} opcode_t;

void printState(stateType *);
int executeInstruction(stateType *state, inst_t inst);

int num_executedInstructions;

static inline opcode_t fetchOpcode(inst_t inst) {
  return ((1 << 3) - 1) & (inst >> 22);
}

static inline int fetchRegA(inst_t inst) {
  return ((1 << 3) - 1) & (inst >> 19);
}

static inline int fetchRegB(inst_t inst) {
  return ((1 << 3) - 1) & (inst >> 16);
}

static inline int fetchDestReg(inst_t inst) { return ((1 << 3) - 1) & (inst); }

static inline int fetchOffset(inst_t inst) {
  /* convert a 16-bit number into a 32-bit Linux integer */
  const int offset = inst & 0xFFFFu;
  return offset & (1 << 15) ? offset - (1 << 16) : offset;
}

int main(int argc, char *argv[]) {
  char line[MAXLINELENGTH];
  stateType state;

  FILE *filePtr;

  if (argc != 2) {
    printf("error: usage: %s <machine-code file>\n", argv[0]);
    exit(1);
  }

  filePtr = fopen(argv[1], "r");
  if (filePtr == NULL) {
    printf("error: can't open file %s", argv[1]);
    perror("fopen");
    exit(1);
  }

  /* read in the entire machine-code file into memory */
  for (state.numMemory = 0; fgets(line, MAXLINELENGTH, filePtr) != NULL;
       state.numMemory++) {
    if (sscanf(line, "%d", state.mem + state.numMemory) != 1) {
      printf("error in reading address %d\n", state.numMemory);
      exit(1);
    }
    printf("memory[%d]=%d\n", state.numMemory, state.mem[state.numMemory]);
  }

  state.pc = 0;
  memset(state.reg, 0, sizeof(state.reg));

  int ret = 0;
  do {
    if (state.pc < 0 || state.pc > state.numMemory) {
      printf("error in decoding pc\n");
      exit(1);
    }
    if (ret < 0) {
      printf("error in decoding instruction\n");
      exit(1);
    }
    printState(&state);
    num_executedInstructions++;
  } while ((ret = executeInstruction(&state, (inst_t)state.mem[state.pc])));
  printState(&state);
  return (0);
}

int executeInstruction(stateType *state, inst_t inst) {
  opcode_t opcode = fetchOpcode(inst);
  ++state->pc;
  switch (opcode) {
  case ADD:
    state->reg[fetchDestReg(inst)] =
        state->reg[fetchRegA(inst)] + state->reg[fetchRegB(inst)];
    break;
  case NOR:
    state->reg[fetchDestReg(inst)] =
        ~(state->reg[fetchRegA(inst)] | state->reg[fetchRegB(inst)]);
    break;
  case LW:
    state->reg[fetchRegB(inst)] =
        state->mem[state->reg[fetchRegA(inst)] + fetchOffset(inst)];
    break;
  case SW:
    state->mem[state->reg[fetchRegA(inst)] + fetchOffset(inst)] =
        state->reg[fetchRegB(inst)];
    break;
  case BEQ:
    if (state->reg[fetchRegA(inst)] == state->reg[fetchRegB(inst)])
      state->pc += fetchOffset(inst);
    break;
  case JALR:
    state->reg[fetchRegB(inst)] = state->pc;
    state->pc = state->reg[fetchRegA(inst)];
    break;
  case HALT:
    printf("machine halted\n");
    printf("total of %d instructions executed\n", num_executedInstructions + 1);
    printf("final state of machine:");
    return 0;
  case NOOP:
    break;
  default:
    return -1;
  }

  return 1;
}

void printState(stateType *statePtr) {
  int i;
  printf("\n@@@\nstate:\n");
  printf("\tpc %d\n", statePtr->pc);
  printf("\tmemory:\n");
  for (i = 0; i < statePtr->numMemory; i++) {
    printf("\t\tmem[ %d ] %d\n", i, statePtr->mem[i]);
  }
  printf("\tregisters:\n");
  for (i = 0; i < NUMREGS; i++) {
    printf("\t\treg[ %d ] %d\n", i, statePtr->reg[i]);
  }
  printf("end state\n");
}