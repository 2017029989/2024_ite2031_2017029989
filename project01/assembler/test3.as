    lw 0 1 answer
	lw 0 2 arr00
	lw 0 3 arr01
sub	noop		sub(reg2, reg3) { reg2 -= reg3 }
	nor 0 3 3	reg3 = ~reg3
	lw 0 4 one	reg4 = 1
	add 3 4 3	reg3 = reg3 + reg4 == -reg3
	add 2 3 2	reg2 = reg2 - reg3
gteq lw 0 3 mask load mask for gt and eq operation
and noop        and(reg2, reg3) = reg2 & reg3
	nor 2 2 2	reg2 = nor(reg2, reg2) = ~reg2
	nor 3 3 3	reg3 = nor(reg3, reg3) = ~reg3
	nor 2 3 2	reg2 = nor(~reg2, ~reg3) = reg2 & reg3
    lw 0 3 zero	reg3 = 0
	beq 2 3 3   if reg2 & reg3 == 0 
    lw 0 2 zero
    beq 0 1 eval
    lw 0 2 one
eval beq 1 2 suc if(arr00 > arr01) goto suc else loop
    noop
	beq 0 0 -1
suc	halt
zero	.fill 0
one		.fill 1
mask	.fill -32768
answer  .fill 1
arr00	.fill -15332
arr01	.fill -860037
