    lw 0 1 answer
    lw 0 2 #A
    lw 0 3 #B
sub noop        sub(reg2, reg3) { reg2 -= reg3 }
	nor 0 3 3	reg3 = ~reg3
	lw 0 4 one	reg4 = 1
	add 3 4 3	reg3 = reg3 + reg4 == -reg3
	add 2 3 2	reg2 = reg2 - reg3
    beq 1 2 succ
    noop
    beq 0 0 -1
succ    halt
answer  .fill 42000
#A  .fill 54321
#B  .fill 12321
one		.fill 1
