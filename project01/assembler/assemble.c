/* Assembler code fragment for LC-2K */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define DOUBLE(x) x x
#define QUAD(x) DOUBLE(DOUBLE(x))

#define MAXLINELENGTH 1000
#define NUMMEMORY 65536 /* maximum number of words in memory */
#define NUMREGS 8       /* number of machine registers */

typedef struct stateStruct {
  int pc;
  int mem[NUMMEMORY];
  int reg[NUMREGS];
  int numMemory;
} stateType;
typedef unsigned int inst_t;
typedef enum OPCODE {
  /* R-type */ ADD,
  /* R-type */ NOR,
  /* I-type */ LW,
  /* I-type */ SW,
  /* I-type */ BEQ,
  /* J-type */ JALR,
  /* O-type */ HALT,
  /* O-type */ NOOP,
  /* directive */ FILL,
} opcode_t;

typedef struct labelDictStruct {
  struct labelDictStruct *next;
  char label[8];
  int addr;
} labelDict_t;

labelDict_t *labelDict;

void initLabelDict(labelDict_t **p_dict);
int addLabelDict(labelDict_t **p_dict, char const *label, int addr);
int lookupLabelDict(labelDict_t const *dict, char const *label);
void destroyLabelDict(labelDict_t *dict);
void printLabelDict(labelDict_t const *dict);

int readAndParse(FILE *, char *, char *, char *, char *, char *);
int isNumber(char const *, int *);

opcode_t decodeOpcode(const char *opcode);
inst_t buildInstruction(int pc, opcode_t op, const char *arg0, const char *arg1,
                        const char *arg2);

static inline inst_t setOpcode(inst_t inst, int opcode) {
  return inst | ((opcode & 7) << 22);
}

static inline inst_t setRegA(inst_t inst, int regIdx) {
  return inst | ((regIdx & 7) << 19);
}

static inline inst_t setRegB(inst_t inst, int regIdx) {
  return inst | ((regIdx & 7) << 16);
}

static inline inst_t setDestReg(inst_t inst, int regIdx) {
  return inst | (regIdx & 7);
}

static inline inst_t setOffset(inst_t inst, int offset) {
  /* convert a 32-bit number into a 16-bit Linux integer */
  return inst | (offset & 0xFFFFu);
}

static inline int isValidRegister(int regIdx) {
  return regIdx >= 0 && regIdx < NUMREGS;
}

static inline int isValidOffset(int offset) {
  return (offset < (1 << 16)) & (offset >= -(1 << 16));
}

int main(int argc, char *argv[]) {
  char *inFileString, *outFileString;
  FILE *inFilePtr, *outFilePtr;
  char label[MAXLINELENGTH], opcode[MAXLINELENGTH], arg0[MAXLINELENGTH],
      arg1[MAXLINELENGTH], arg2[MAXLINELENGTH];

  if (argc != 3) {
    printf("error: usage: %s <assembly-code-file> <machine-code-file>\n",
           argv[0]);
    exit(1);
  }

  inFileString = argv[1];
  outFileString = argv[2];

  if ((inFilePtr = fopen(inFileString, "r")) == NULL) {
    printf("error in opening %s\n", inFileString);
    exit(1);
  }

  if ((outFilePtr = fopen(outFileString, "w")) == NULL) {
    printf("error in opening %s\n", outFileString);
    exit(1);
  }

  // Initialize the dictionary for the labels
  initLabelDict(&labelDict);

  // Phase 1. Register labels
  int pc = 0;
  while (readAndParse(inFilePtr, label, opcode, arg0, arg1, arg2)) {
    int label_len = strlen(label);

    if (label_len > 6) {
      printf("label with more than 6 characters\n");
      exit(1);
    }

    if (label_len > 0) {
      // register a label
      int err;
      if ((err = addLabelDict(&labelDict, label, pc))) {
        if (err == -2) {
          printf("duplicate labels\n");
        }
        exit(1);
      }
    }
    pc++;
  }

  /* this is how to rewind the file ptr so that you start reading from the
      beginning of the file */
  rewind(inFilePtr);

  // Phase 2. Assemble the results
  pc = 0;
  inst_t inst;
  opcode_t op;
  while (readAndParse(inFilePtr, label, opcode, arg0, arg1, arg2)) {
    if ((op = decodeOpcode(opcode)) == -1) {
      printf("unrecognized opcodes\n");
      exit(1);
    }
    inst = buildInstruction(pc, op, arg0, arg1, arg2);
    fprintf(outFilePtr, "%d\n", inst);
    pc++;
  }

  destroyLabelDict(labelDict);

  return (0);
}

/*
 * Read and parse a line of the assembly-language file.  Fields are returned
 * in label, opcode, arg0, arg1, arg2 (these strings must have memory already
 * allocated to them).
 *
 * Return values:
 *     0 if reached end of file
 *     1 if all went well
 *
 * exit(1) if line is too long.
 */
int readAndParse(FILE *inFilePtr, char *label, char *opcode, char *arg0,
                 char *arg1, char *arg2) {
  char line[MAXLINELENGTH];
  char *ptr = line;

  /* delete prior values */
  label[0] = opcode[0] = arg0[0] = arg1[0] = arg2[0] = '\0';

  /* read the line from the assembly-language file */
  if (fgets(line, MAXLINELENGTH, inFilePtr) == NULL) {
    /* reached end of file */
    return (0);
  }

  /* check for line too long (by looking for a \n) */
  if (strchr(line, '\n') == NULL) {
    /* line too long */
    printf("error: line too long\n");
    exit(1);
  }

  /* is there a label? */
  ptr = line;
  if (sscanf(ptr, "%[^\t\n\r ]", label)) {
    /* successfully read label; advance pointer over the label */
    ptr += strlen(label);
  }

  /*
   * Parse the rest of the line.  Would be nice to have real regular
   * expressions, but scanf will suffice.
   */
  sscanf(ptr, QUAD("%*[\t\n\r ]%[^\t\n\r ]"), opcode, arg0, arg1, arg2);

  return (1);
}

int isNumber(char const *string, int *n) { /* return 1 if string is a number */
  return ((sscanf(string, "%d", n)) == 1);
}

opcode_t decodeOpcode(const char *opcode) {
  opcode_t op;
  if (strcmp(".fill", opcode) == 0) {
    op = FILL;
  } else if (strcmp("add", opcode) == 0) {
    op = ADD;
  } else if (strcmp("nor", opcode) == 0) {
    op = NOR;
  } else if (strcmp("lw", opcode) == 0) {
    op = LW;
  } else if (strcmp("sw", opcode) == 0) {
    op = SW;
  } else if (strcmp("beq", opcode) == 0) {
    op = BEQ;
  } else if (strcmp("jalr", opcode) == 0) {
    op = JALR;
  } else if (strcmp("halt", opcode) == 0) {
    op = HALT;
  } else if (strcmp("noop", opcode) == 0) {
    op = NOOP;
  } else {
    op = -1;
  }
  return op;
}

void initLabelDict(labelDict_t **p_dict) {
  *p_dict = malloc(sizeof(labelDict_t));
  labelDict_t *dict = *p_dict;
  dict->next = 0;
  dict->label[0] = 0;
  dict->addr = 0;
}

int addLabelDict(labelDict_t **p_dict, char const *label, int addr) {
  labelDict_t *dict = *p_dict;
  if (!dict)
    return -1; // empty dict
  if (lookupLabelDict(dict, label) >= 0)
    return -2; // duplicate
  int label_len = strlen(label);
  if (label_len > 6)
    return -3; // label is too long
  labelDict_t *newDictItem = malloc(sizeof(labelDict_t));
  if (!newDictItem)
    return -4; // not enough memory
  strncpy(newDictItem->label, label, label_len + 1);
  newDictItem->addr = addr;
  newDictItem->next = dict;
  (*p_dict) = newDictItem;
  return 0;
}

int lookupLabelDict(labelDict_t const *dict, char const *label) {
  if (!dict)
    return -2; // empty dict
  do {
    if (!strcmp(label, dict->label))
      return dict->addr;
  } while ((dict = dict->next));
  return -1; // not found
}

void destroyLabelDict(labelDict_t *dict) {
  if (!dict)
    return;

  if (dict->next)
    destroyLabelDict(dict->next);

  free(dict);
}

void printLabelDict(labelDict_t const *dict) {
  if (!dict)
    return;
  printf("%s %d\n", dict->label, dict->addr);
  if (dict->next)
    printLabelDict(dict->next);
}

inst_t buildInstruction(int pc, opcode_t op, const char *arg0, const char *arg1,
                        const char *arg2) {
  int regA, regB, regDest, offset;
  inst_t inst = 0;

  if (op == ADD | op == NOR) {
    if (!isNumber(arg0, &regA) || !isNumber(arg1, &regB) ||
        !isNumber(arg2, &regDest) || !isValidRegister(regA) ||
        !isValidRegister(regB) || !isValidRegister(regDest)) {
      printf("invalid register values\n");
      exit(1);
    }
    inst = setOpcode(inst, op);
    inst = setRegA(inst, regA);
    inst = setRegB(inst, regB);
    inst = setDestReg(inst, regDest);
  } else if (op == LW | op == SW | op == BEQ) {
    if (!isNumber(arg0, &regA) || !isNumber(arg1, &regB) ||
        !isValidRegister(regA) || !isValidRegister(regB)) {
      printf("invalid register values\n");
      exit(1);
    }
    if (!isNumber(arg2, &offset)) {
      if ((offset = lookupLabelDict(labelDict, arg2)) < 0) {
        printf("undefined labels\n");
        exit(1);
      }
      if (op == BEQ)
        offset = offset - (pc + 1);

      if (!isValidOffset(offset)) {
        printf("offsetFields that doesn't fit in 16bits\n");
        exit(1);
      }
    }
    inst = setOpcode(inst, op);
    inst = setRegA(inst, regA);
    inst = setRegB(inst, regB);
    inst = setOffset(inst, offset);
  } else if (op == JALR) {
    if (!isNumber(arg0, &regA) || !isNumber(arg1, &regB) ||
        !isValidRegister(regA) || !isValidRegister(regB)) {
      printf("invalid register values\n");
      exit(1);
    }
    inst = setOpcode(inst, op);
    inst = setRegA(inst, regA);
    inst = setRegB(inst, regB);
  } else if (op == HALT || op == NOOP) {
    inst = setOpcode(inst, op);
  } else if (op == FILL) {
    if (!isNumber(arg0, &offset)) {
      if ((offset = lookupLabelDict(labelDict, arg0)) < 0) {
        printf("undefined labels\n");
        exit(1);
      }
    }
    inst = offset;
  }
  return inst;
}