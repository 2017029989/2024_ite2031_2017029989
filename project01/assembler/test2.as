    lw 0 1 answer
    lw 0 2 #A
    lw 0 3 #B
and noop        and(reg2, reg3) = reg2 & reg3
	nor 2 2 2	reg2 = nor(reg2, reg2) = ~reg2
	nor 3 3 3	reg3 = nor(reg3, reg3) = ~reg3
	nor 2 3 2	reg2 = nor(~reg2, ~reg3) = reg2 & reg3
    noop        Check the answer
    beq 1 2 succ
    noop
    beq 0 0 -1
succ halt
answer  .fill 4129
#A  .fill 54321
#B  .fill 12321
mask	.fill -32768
